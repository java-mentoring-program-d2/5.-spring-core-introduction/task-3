package service;

import config.AppConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class EmployeeServiceTest {
    private EmployeeService employeeService;

    public EmployeeServiceTest() {
    }

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        employeeService = context.getBean(EmployeeService.class);
    }

    @Test
    public void fireRandomlyAt() {
        Assert.assertNotNull(employeeService);
        System.out.println("Test application context started!");
    }

}