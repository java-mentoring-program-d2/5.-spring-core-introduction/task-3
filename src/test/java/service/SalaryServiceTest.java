package service;

import config.AppConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SalaryServiceTest {
    private SalaryService salaryService;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        salaryService = context.getBean(SalaryService.class);
    }

    @Test
    public void applicationContext() {
        Assert.assertNotNull(salaryService);
        System.out.println("Test application context started!");
    }
}