package service;

import config.AppConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PositionServiceTest {
    private PositionService positionService;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        positionService = context.getBean(PositionService.class);
    }

    @Test
    public void getAvailablePositions() {
        Assert.assertNotNull(positionService);
        System.out.println("Test application context started!");
    }
}