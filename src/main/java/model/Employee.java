package model;

public class Employee {
    private static int count = 1;

    private int id;
    private String name;
    private int age;
    private String skill;
    private int skillLevel;
    private int expectedSalaryMinimum;
    private Position position;

    public Employee() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Employee(String name, int age, String skill, int skillLevel, int expectedSalaryMinimum) {
        this.id = count++;
        this.name = name;
        this.age = age;
        this.skill = skill;
        this.skillLevel = skillLevel;
        this.expectedSalaryMinimum = expectedSalaryMinimum;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSkill() {
        return skill;
    }

    public int getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(int skillLevel) {
        this.skillLevel = skillLevel;
    }

    public int getExpectedSalaryMinimum() {
        return expectedSalaryMinimum;
    }

    public void setExpectedSalaryMinimum(int expectedSalaryMinimum) {
        this.expectedSalaryMinimum = expectedSalaryMinimum;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
