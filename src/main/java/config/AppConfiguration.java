package config;

import dao.EmployeeDao;
import dao.PositionDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import service.EmployeeService;
import service.PositionService;
import service.SalaryService;

@Configuration
public class AppConfiguration {

    @Bean
    public EmployeeService employeeService() {
        return new EmployeeService(positionService(), salaryService(), employeeDao());
    }

    @Bean
    public PositionService positionService() {
        return new PositionService(positionDao());
    }

    @Bean
    public SalaryService salaryService() {
        return new SalaryService();
    }

    @Bean
    public EmployeeDao employeeDao() {
        return new EmployeeDao();
    }

    @Bean
    public PositionDao positionDao() {
        return new PositionDao();
    }
}
